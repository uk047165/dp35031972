docker-compose down
docker volume rm database_ha08
docker volume rm 35031972_data_warehouse
docker volume rm 35031972_data_shop

docker image rm dp35031972_warehouseserver
docker image rm dp35031972_shopserver

docker container rm dp35031972_warehouseserver
docker container rm dp35031972_shopserver
