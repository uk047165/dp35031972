package ha09;

import org.fulib.yaml.Yamler;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static ha08.Utils.*;
import static org.fulib.yaml.EventSource.EVENT_KEY;

// HA08: 0/10 ==> kein Test
// HA09: 10/10
// HA10: 10/10
public class TestDocker {

//    public static DockerComposeContainer compose =
//            new DockerComposeContainer(new File("docker-compose.yaml"))
//                    .withExposedService("shopserver", 8000, Wait.forListeningPort().withStartupTimeout(Duration.ofSeconds(60)))
//                    .withExposedService("warehouseserver", 8001,Wait.forListeningPort().withStartupTimeout(Duration.ofSeconds(60)))
//                    .withLocalCompose(true);

    @Test
    public void testCompose() throws Exception {

        // docker-compose up muss unter Umständen manuell im Verzeichnis dp3503192 ausgeführt werden,
        // das manchmal aber auch nicht, dann funktioniert es aber auch wenn man den Test abbricht
        // und nochmal startet. Wer weiß schon was passiert?

        composeUp();
//        compose.start();

        sleep(1);

        System.err.println("WAIT TILL THE SERVERS ARE ONLINE: ...");
        waitTillServerOnline(SHOP_SERVER_PORT);
        waitTillServerOnline(WAREHOUSE_SERVER_PORT);

        sleep(10);

        System.out.println("Send Request 1");
        String s = sendRequest("http://localhost:" + SHOP_SERVER_PORT + "/ping", "");
        Assert.assertEquals("OK\n", s);

        System.out.println("Send Request 2");
        String s1 = sendRequest("http://localhost:" + WAREHOUSE_SERVER_PORT + "/ping", "");
        Assert.assertEquals("OK\n", s1);

        String s2 = sendRequest("http://localhost:" + WAREHOUSE_SERVER_PORT + "/addLot", "");
        Assert.assertEquals("Lot added!\n", s2);

        String s3 = sendRequest("http://localhost:" + WAREHOUSE_SERVER_PORT + "/getShopEvents", "");
        ArrayList<LinkedHashMap<String, String>> linkedHashMaps = new Yamler().decodeList(s3);
        LinkedHashMap<String, String> map = linkedHashMaps.get(linkedHashMaps.size() - 1);
        System.out.println(map);
        Assert.assertEquals("testID", map.get(EVENT_KEY));
        Assert.assertEquals("testP", map.get(PRODUCT_NAME));
        Assert.assertEquals(42, Integer.parseInt(map.get(SIZE)));

        sleep(2);

        String s4 = sendRequest("http://localhost:" + SHOP_SERVER_PORT + "/getLastProduct", "");
        System.out.println(s4);
        ArrayList<LinkedHashMap<String, String>> mapList = new Yamler().decodeList(s4);
        LinkedHashMap<String, String> map2 = mapList.get(mapList.size() - 1);
        Assert.assertEquals("testP", map2.get(PRODUCT_NAME));
        Assert.assertEquals(42, Integer.parseInt(map2.get(SIZE)));

        sleep(5);
        composeDown();
    }

    public void composeDown() throws Exception {
        Runtime.getRuntime().exec("docker-compose down");
    }

    public void composeUp() throws Exception {
        System.err.println("#-#-#-#-#-#\nSTART DOCKER\n#-#-#-#-#-#");
//        outputTerminalMessages(p1);


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Process p1 = Runtime.getRuntime().exec("docker-compose up -d");
                } catch (Exception e) {

                }
            }
        }).run();
    }

    private void waitTillServerOnline(int port) throws InterruptedException {
        while (true) {
            sleep(5);

            String s = sendRequest("http://localhost:" + port + "/ping", "", true);

            if ("OK\n".equals(s)) {
                String delim = "#-#-#-#-#-#-#-#-#-#";
                if (port == SHOP_SERVER_PORT) {
                    Logger.getGlobal().info("\n" + delim + "\nSHOP SERVER ONLINE.\n" + delim);
                } else if (port == WAREHOUSE_SERVER_PORT) {
                    Logger.getGlobal().info("\n" + delim + "\nWAREHOUSE SERVER ONLINE.\n" + delim);
                }
                return;
            }
        }
    }

    private void sleep(int sec) throws InterruptedException {
        TimeUnit.SECONDS.sleep(sec);
    }

    private void outputTerminalMessages(Process p) throws IOException {

        System.out.println("\n" + p + "\n");

        InputStream inputStream = p.getInputStream();
//        InputStream inputStream = p.getErrorStream();
        BufferedReader buf = new BufferedReader(new InputStreamReader(inputStream));

        while (true) {
            String line = buf.readLine();
            if (line == null) {
                break;
            }

            System.out.println(line);
        }
    }
}
