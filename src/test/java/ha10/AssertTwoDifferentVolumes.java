package ha10;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import static ha08.Utils.*;
import static java.lang.Thread.sleep;

public class AssertTwoDifferentVolumes {

    @Test
    public void testTestTest() throws Exception {

        // docker-compose up muss unter Umständen manuell im Verzeichnis dp3503192 ausgeführt werden,
        // das manchmal aber auch nicht, dann funktioniert es aber auch wenn man den Test abbricht
        // und nochmal startet. Wer weiß schon was passiert?

        composeUp();

        sleep(1);

        System.err.println("WAIT TILL THE SERVERS ARE ONLINE: ...");
        waitTillServerOnline(SHOP_SERVER_PORT);
        waitTillServerOnline(WAREHOUSE_SERVER_PORT);

        Process p = Runtime.getRuntime().exec("docker container inspect server_shop");
        String output = getOutput(p);

        Process p1 = Runtime.getRuntime().exec("docker container inspect server_warehouse");
        String output1 = getOutput(p1);

        String volume1 = getVolumeName(output);
        String volume2 = getVolumeName(output1);

        System.out.println("Shop Volume: " + volume1);
        System.out.println("Warehouse Volume: " + volume2);

        Assert.assertNotEquals(volume1, volume2);

    }

    private void waitTillServerOnline(int port) throws InterruptedException {
        while (true) {
            sleep(5);

            String s = sendRequest("http://localhost:" + port + "/ping", "", true);

            if ("OK\n".equals(s)) {
                String delim = "#-#-#-#-#-#-#-#-#-#";
                if (port == SHOP_SERVER_PORT) {
                    Logger.getGlobal().info("\n" + delim + "\nSHOP SERVER ONLINE.\n" + delim);
                } else if (port == WAREHOUSE_SERVER_PORT) {
                    Logger.getGlobal().info("\n" + delim + "\nWAREHOUSE SERVER ONLINE.\n" + delim);
                }
                return;
            }
        }
    }

    public void composeUp() throws Exception {
        System.err.println("#-#-#-#-#-#\nSTART DOCKER\n#-#-#-#-#-#");
//        outputTerminalMessages(p1);


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Process p1 = Runtime.getRuntime().exec("docker-compose up -d");
                } catch (Exception e) {

                }
            }
        }).run();
    }

    private String getVolumeName(String output) throws Exception {
        Gson gson = new Gson();
        JsonObject[] jsonObjects = gson.fromJson(output, JsonObject[].class);

        JsonObject[] mounts = gson.fromJson(jsonObjects[0].get("Mounts").toString(), JsonObject[].class);
        return mounts[0].get("Name").toString();
    }

    private String getOutput(Process p) throws Exception {
        InputStream inputStream = p.getInputStream();
//        InputStream inputStream = p.getErrorStream();
        BufferedReader buf = new BufferedReader(new InputStreamReader(inputStream));

        StringBuilder sb = new StringBuilder();

        while (true) {
            String line = buf.readLine();
            if (line == null) {
                break;
            }
            sb.append(line).append("\n");
        }

        return sb.toString();
    }
}
