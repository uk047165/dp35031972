package ha08.warehouse51;

import ha08.warehouse51.model.*;
import org.fulib.yaml.EventFiler;
import org.fulib.yaml.EventSource;
import org.fulib.yaml.Yamler;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static ha08.Utils.*;
import static org.fulib.yaml.EventSource.EVENT_KEY;
import static org.fulib.yaml.EventSource.EVENT_TYPE;

public class WarehouseBuilder {

    private EventSource eventSource;


    private Warehouse51 warehouse51;
    public ShopProxy shopProxy;

    public WarehouseBuilder() {
        warehouse51 = new Warehouse51();
        shopProxy = new ShopProxy();
        eventSource = new EventSource();

        // create some PalettePlaces to put products
        for (int i = 0; i < 6; i++) {
            PalettePlace p = new PalettePlace();
            p.setWarehouse(warehouse51);
            p.setColumn(i);
            p.setRow(42);
            p.setId(String.format("palette%dx%d", i, 42));
        }

        // start logging
        EventFiler eventFiler = new EventFiler(eventSource)
                .setHistoryFileName(WAREHOUSE_BUILDER_LOG);

        // restore stuff from log
        String yaml = eventFiler.loadHistory();
        if (yaml != null) {
            ArrayList<LinkedHashMap<String, String>> eventList
                    = new Yamler().decodeList(yaml);
            applyEvents(eventList);
        }

        eventFiler.startEventLogging();
    }

    public void shipOrder(String productName, String address, String orderID) {

        // logging this
        LinkedHashMap<String, String> event = new LinkedHashMap<>();
        event.put(EVENT_TYPE, SHIP_ORDER);
        event.put(EVENT_KEY, orderID);
        event.put(PRODUCT_NAME, productName);
        event.put(ADDRESS, address);

        eventSource.append(event);

        // check if order exists already
        for(WarehouseOrder o : warehouse51.getOrders()){
            if (o.getId().equals(orderID)){
                return;
            }
        }

        //  get product
        String productID = productName.replaceAll("\\W", "");
        WarehouseProduct product = null;

        // search if product with id exists
        for (WarehouseProduct p : warehouse51.getProducts()) {
            if (p.getId().equals(productID)) {
                product = p;
                break;
            }
        }

        if (product == null) {
            System.err.println("ERROR: Product " + productID + " unknown!");
            return;
        }

        // create Warehouseorder
        WarehouseOrder order = getOrCreateOrder(orderID);
        order.setId(orderID);
        order.setAddress(address);
        order.setWarehouse(warehouse51);
        // add order to product
        order.setProduct(product);


        // increment stock
        for (Lot l : product.getLots()) {
            if (l.getLotSize() > 0) {
                int oldStock = l.getLotSize();
                l.setLotSize(oldStock - 1);
                break;
            }
        }

        // "shipping" product to customer is not possible to implement
    }

    private WarehouseOrder getOrCreateOrder(String orderID) {

        for (WarehouseOrder o : warehouse51.getOrders()) {
            if (o.getId().equals(orderID)) {
                return o;
            }
        }

        WarehouseOrder newOrder = new WarehouseOrder();
        newOrder.setId(orderID);
        return newOrder;
    }

    public void applyEvents(ArrayList<LinkedHashMap<String, String>> eventList) {
        // TODO change to chanin of responsibility
        for (LinkedHashMap<String, String> event : eventList) {
            if (ADD_LOT_TO_STOCK.equals(event.get(EVENT_TYPE))) {

                String lotID = event.get(EVENT_KEY);
                String prodcutName = event.get(PRODUCT_NAME);
                int size = Integer.parseInt(event.get(SIZE));
                addLotToStock(lotID, prodcutName, size);
            } else if (SHIP_ORDER.equals(event.get(EVENT_TYPE))) {

                String productName = event.get(PRODUCT_NAME);
                String orderID = event.get(EVENT_KEY);
                String address = event.get(ADDRESS);
                shipOrder(productName, address, orderID);
            }
        }
    }

    public void addLotToStock(String lotID, String productName, int size) {

        if (lotExists(lotID)) {
            // id should uniquely identify a lot
            // we could also check if we already have a lot with *all* the same properties; to handle doubled messages
            return;
        }

        WarehouseProduct product = getOrCreateProduct(productName);

        Lot lot = new Lot();
        lot.setId(lotID);
        lot.setProduct(product);
        lot.setLotSize(size);

        // find place for lot and place it
        for (PalettePlace place : warehouse51.getPalettePlaces()) {
            if (place.getLot() == null) {
                lot.setPalettePlace(place);
                break;
            }
        }

        LinkedHashMap<String, String> event = new LinkedHashMap<>();
        event.put(EVENT_TYPE, ADD_LOT_TO_STOCK);
        event.put(EVENT_KEY, lotID);
        event.put(PRODUCT_NAME, productName);
        event.put(SIZE, "" + size);

        eventSource.append(event);

        // tell shopProxy about method call
        shopProxy.addLotToShop(lotID, productName, size);

    }

    /**
     * Checks if the product is already known to the warehouse. Returns the existing product or a new product.
     *
     * @param productName The name to search for products
     * @return Existing product if it exists. A new product if no product exists with the given name.
     */
    private WarehouseProduct getOrCreateProduct(String productName) {

        String productID = productName.replaceAll("\\W", "");

        // search if product with id exists
        for (WarehouseProduct p : warehouse51.getProducts()) {
            if (p.getId().equals(productID)) {
                return p;
            }
        }

        // else return new product
        WarehouseProduct product = new WarehouseProduct();
        product.setId(productID);
        product.setName(productName);
        product.setWarehouse(warehouse51);

        return product;
    }

    /**
     * Checks if a request / lot with the given id already exists somewhere in the warehouse.
     *
     * @param lotID The id to be checked
     * @return True if the lot already exists. False if not
     */
    private boolean lotExists(String lotID) {

        for (PalettePlace place : warehouse51.getPalettePlaces()) {
            if (place.getLot() != null) {
                if (place.getLot().getId().equals(lotID)) {
                    return true;
                }
            }
        }

        return false;
    }

    private Lot getOrCreateLot(String lotID) {

        // search if it already exists
        for (PalettePlace palette : warehouse51.getPalettePlaces()) {
            Lot thisLot = palette.getLot();
            if (thisLot.getId().equals(lotID)) {
                return thisLot;
            }
        }

        // else return new
        Lot lot = new Lot();
        lot.setId(lotID);

        return lot;
    }

    public ArrayList<WarehouseProduct> getProducts() {
        return warehouse51.getProducts();
    }

    public ArrayList<WarehouseOrder> getOrders() {
        return warehouse51.getOrders();
    }
}
