package ha08.warehouse51.model;

import java.beans.PropertyChangeSupport;

import java.beans.PropertyChangeListener;

public class WarehouseOrder  
{

   public static final String PROPERTY_address = "address";

   private String address;

   public String getAddress()
   {
      return address;
   }

   public WarehouseOrder setAddress(String value)
   {
      if (value == null ? this.address != null : ! value.equals(this.address))
      {
         String oldValue = this.address;
         this.address = value;
         firePropertyChange("address", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_id = "id";

   private String id;

   public String getId()
   {
      return id;
   }

   public WarehouseOrder setId(String value)
   {
      if (value == null ? this.id != null : ! value.equals(this.id))
      {
         String oldValue = this.id;
         this.id = value;
         firePropertyChange("id", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_warehouse = "warehouse";

   private Warehouse51 warehouse = null;

   public Warehouse51 getWarehouse()
   {
      return this.warehouse;
   }

   public WarehouseOrder setWarehouse(Warehouse51 value)
   {
      if (this.warehouse != value)
      {
         Warehouse51 oldValue = this.warehouse;
         if (this.warehouse != null)
         {
            this.warehouse = null;
            oldValue.withoutOrders(this);
         }
         this.warehouse = value;
         if (value != null)
         {
            value.withOrders(this);
         }
         firePropertyChange("warehouse", oldValue, value);
      }
      return this;
   }



   public static final String PROPERTY_product = "product";

   private WarehouseProduct product = null;

   public WarehouseProduct getProduct()
   {
      return this.product;
   }

   public WarehouseOrder setProduct(WarehouseProduct value)
   {
      if (this.product != value)
      {
         WarehouseProduct oldValue = this.product;
         if (this.product != null)
         {
            this.product = null;
            oldValue.withoutOrders(this);
         }
         this.product = value;
         if (value != null)
         {
            value.withOrders(this);
         }
         firePropertyChange("product", oldValue, value);
      }
      return this;
   }



   protected PropertyChangeSupport listeners = null;

   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null)
      {
         listeners.firePropertyChange(propertyName, oldValue, newValue);
         return true;
      }
      return false;
   }

   public boolean addPropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(listener);
      return true;
   }

   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(propertyName, listener);
      return true;
   }

   public boolean removePropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(listener);
      }
      return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(propertyName, listener);
      }
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();

      result.append(" ").append(this.getAddress());
      result.append(" ").append(this.getId());


      return result.substring(1);
   }

   public void removeYou()
   {
      this.setWarehouse(null);
      this.setProduct(null);

   }


}