package ha08.warehouse51.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Warehouse51  
{

   public static final java.util.ArrayList<WarehouseProduct> EMPTY_products = new java.util.ArrayList<WarehouseProduct>()
   { @Override public boolean add(WarehouseProduct value){ throw new UnsupportedOperationException("No direct add! Use xy.withProducts(obj)"); }};


   public static final String PROPERTY_products = "products";

   private java.util.ArrayList<WarehouseProduct> products = null;

   public java.util.ArrayList<WarehouseProduct> getProducts()
   {
      if (this.products == null)
      {
         return EMPTY_products;
      }

      return this.products;
   }

   public Warehouse51 withProducts(Object... value)
   {
      if(value==null) return this;
      for (Object item : value)
      {
         if (item == null) continue;
         if (item instanceof java.util.Collection)
         {
            for (Object i : (java.util.Collection) item)
            {
               this.withProducts(i);
            }
         }
         else if (item instanceof WarehouseProduct)
         {
            if (this.products == null)
            {
               this.products = new java.util.ArrayList<WarehouseProduct>();
            }
            if ( ! this.products.contains(item))
            {
               this.products.add((WarehouseProduct)item);
               ((WarehouseProduct)item).setWarehouse(this);
               firePropertyChange("products", null, item);
            }
         }
         else throw new IllegalArgumentException();
      }
      return this;
   }



   public Warehouse51 withoutProducts(Object... value)
   {
      if (this.products == null || value==null) return this;
      for (Object item : value)
      {
         if (item == null) continue;
         if (item instanceof java.util.Collection)
         {
            for (Object i : (java.util.Collection) item)
            {
               this.withoutProducts(i);
            }
         }
         else if (item instanceof WarehouseProduct)
         {
            if (this.products.contains(item))
            {
               this.products.remove((WarehouseProduct)item);
               ((WarehouseProduct)item).setWarehouse(null);
               firePropertyChange("products", item, null);
            }
         }
      }
      return this;
   }


   public static final java.util.ArrayList<PalettePlace> EMPTY_palettePlaces = new java.util.ArrayList<PalettePlace>()
   { @Override public boolean add(PalettePlace value){ throw new UnsupportedOperationException("No direct add! Use xy.withPalettePlaces(obj)"); }};


   public static final String PROPERTY_palettePlaces = "palettePlaces";

   private java.util.ArrayList<PalettePlace> palettePlaces = null;

   public java.util.ArrayList<PalettePlace> getPalettePlaces()
   {
      if (this.palettePlaces == null)
      {
         return EMPTY_palettePlaces;
      }

      return this.palettePlaces;
   }

   public Warehouse51 withPalettePlaces(Object... value)
   {
      if(value==null) return this;
      for (Object item : value)
      {
         if (item == null) continue;
         if (item instanceof java.util.Collection)
         {
            for (Object i : (java.util.Collection) item)
            {
               this.withPalettePlaces(i);
            }
         }
         else if (item instanceof PalettePlace)
         {
            if (this.palettePlaces == null)
            {
               this.palettePlaces = new java.util.ArrayList<PalettePlace>();
            }
            if ( ! this.palettePlaces.contains(item))
            {
               this.palettePlaces.add((PalettePlace)item);
               ((PalettePlace)item).setWarehouse(this);
               firePropertyChange("palettePlaces", null, item);
            }
         }
         else throw new IllegalArgumentException();
      }
      return this;
   }



   public Warehouse51 withoutPalettePlaces(Object... value)
   {
      if (this.palettePlaces == null || value==null) return this;
      for (Object item : value)
      {
         if (item == null) continue;
         if (item instanceof java.util.Collection)
         {
            for (Object i : (java.util.Collection) item)
            {
               this.withoutPalettePlaces(i);
            }
         }
         else if (item instanceof PalettePlace)
         {
            if (this.palettePlaces.contains(item))
            {
               this.palettePlaces.remove((PalettePlace)item);
               ((PalettePlace)item).setWarehouse(null);
               firePropertyChange("palettePlaces", item, null);
            }
         }
      }
      return this;
   }


   protected PropertyChangeSupport listeners = null;

   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null)
      {
         listeners.firePropertyChange(propertyName, oldValue, newValue);
         return true;
      }
      return false;
   }

   public boolean addPropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(listener);
      return true;
   }

   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(propertyName, listener);
      return true;
   }

   public boolean removePropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(listener);
      }
      return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(propertyName, listener);
      }
      return true;
   }



   public void removeYou()
   {
      this.withoutProducts(this.getProducts().clone());


      this.withoutPalettePlaces(this.getPalettePlaces().clone());


      this.withoutOrders(this.getOrders().clone());


   }














   public static final java.util.ArrayList<WarehouseOrder> EMPTY_orders = new java.util.ArrayList<WarehouseOrder>()
   { @Override public boolean add(WarehouseOrder value){ throw new UnsupportedOperationException("No direct add! Use xy.withOrders(obj)"); }};


   public static final String PROPERTY_orders = "orders";

   private java.util.ArrayList<WarehouseOrder> orders = null;

   public java.util.ArrayList<WarehouseOrder> getOrders()
   {
      if (this.orders == null)
      {
         return EMPTY_orders;
      }

      return this.orders;
   }

   public Warehouse51 withOrders(Object... value)
   {
      if(value==null) return this;
      for (Object item : value)
      {
         if (item == null) continue;
         if (item instanceof java.util.Collection)
         {
            for (Object i : (java.util.Collection) item)
            {
               this.withOrders(i);
            }
         }
         else if (item instanceof WarehouseOrder)
         {
            if (this.orders == null)
            {
               this.orders = new java.util.ArrayList<WarehouseOrder>();
            }
            if ( ! this.orders.contains(item))
            {
               this.orders.add((WarehouseOrder)item);
               ((WarehouseOrder)item).setWarehouse(this);
               firePropertyChange("orders", null, item);
            }
         }
         else throw new IllegalArgumentException();
      }
      return this;
   }



   public Warehouse51 withoutOrders(Object... value)
   {
      if (this.orders == null || value==null) return this;
      for (Object item : value)
      {
         if (item == null) continue;
         if (item instanceof java.util.Collection)
         {
            for (Object i : (java.util.Collection) item)
            {
               this.withoutOrders(i);
            }
         }
         else if (item instanceof WarehouseOrder)
         {
            if (this.orders.contains(item))
            {
               this.orders.remove((WarehouseOrder)item);
               ((WarehouseOrder)item).setWarehouse(null);
               firePropertyChange("orders", item, null);
            }
         }
      }
      return this;
   }
















}