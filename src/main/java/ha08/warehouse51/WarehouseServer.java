package ha08.warehouse51;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.fulib.yaml.Yamler;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ha08.Utils.*;

public class WarehouseServer {

    private static ExecutorService executor;
    private static ExecutorService threadPool;
    private static long lastKnownWarehouseEventTime = 0;
    private static HttpServer server;
    private static WarehouseBuilder builder;

    public static void main(String[] args) {
        try {
            builder = new WarehouseBuilder();

            executor = Executors.newSingleThreadExecutor();
            threadPool = Executors.newCachedThreadPool();

            server = HttpServer.create(new InetSocketAddress(WAREHOUSE_SERVER_PORT), 0);
            server.setExecutor(executor);
            HttpContext context = server.createContext("/ping");
            context.setHandler(WarehouseServer::handleShopPing);

            HttpContext proxyContext = server.createContext("/getShopEvents");
            proxyContext.setHandler(WarehouseServer::handleShopProxyRequest);

            HttpContext lotContext = server.createContext("/addLot");
            lotContext.setHandler(WarehouseServer::addLotTest);

            server.start();

            // FIXME this is producing exceptions, how to handle this better?
            retrieveNewEventsFromShop();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void addLotTest(HttpExchange e) {
        writeAnswer(e, "Lot added!");

        builder.addLotToStock("testID", "testP", 42);
    }


    private static void retrieveNewEventsFromShop() {

        String shopEvents = sendRequest("http://server_shop:" + SHOP_SERVER_PORT + "/getWarehouseEvents",
                "lastKnown: " + lastKnownWarehouseEventTime); // FIXME change lastKnown to String with datefromat iso date format -> like in yaml

        if (shopEvents != null && builder != null) {
            ArrayList<LinkedHashMap<String, String>> eventList = new Yamler().decodeList(shopEvents);

            executor.execute(() -> builder.applyEvents(eventList));
        }
    }

    private static void handleShopProxyRequest(HttpExchange exchange) {

        String yaml;
        if (builder != null) {

            yaml = builder.shopProxy.getEvents();
            System.out.println("Warehouse Server: send\n" + yaml);

        } else {
            yaml = "Unavailable";
        }
        writeAnswer(exchange, yaml);
    }

    private static void handleShopPing(HttpExchange exchange) {

        String body = getBody(exchange);
        System.out.println("Warehouse Server: got " + body);
        writeAnswer(exchange, "OK");

        retrieveNewEventsFromShop();
    }


    public WarehouseServer() {
        builder = new WarehouseBuilder();
    }

    public void startServer() {
        try {
            server = HttpServer.create(new InetSocketAddress(8001), 0);

            server.createContext("/shipOrder", new shipOrderHandler());

            server.setExecutor(null);

            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopServer() {
        server.stop(0);
    }

    class shipOrderHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            InputStream requestBody = httpExchange.getRequestBody();
            BufferedReader buf = new BufferedReader(new InputStreamReader(requestBody, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();

            while (true) {
                String line = buf.readLine();
                if (line == null) {
                    break;
                }

                sb.append(line).append("\n");
            }

            //  apply events in builder
            String json = sb.toString();

            Gson gson = new Gson();
            Map<String, String> message = gson.fromJson(json, Map.class);

            builder.shipOrder(message.get(PRODUCT_NAME), message.get(ADDRESS), message.get(ORDER_ID));

            // send ok response
            String response = "OK" + httpExchange.getRequestURI() + "\nMessage: " + json + " arrived!";
            httpExchange.sendResponseHeaders(200, response.getBytes().length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    public static WarehouseBuilder getBuilder() {
        return builder;
    }

    public static void setBuilder(WarehouseBuilder b) {
        builder = b;
        retrieveNewEventsFromShop();
    }

}
