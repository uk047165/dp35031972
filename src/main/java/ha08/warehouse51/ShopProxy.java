package ha08.warehouse51;

import com.google.gson.Gson;
import org.fulib.yaml.EventFiler;
import org.fulib.yaml.EventSource;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static ha08.Utils.*;
import static org.fulib.yaml.EventSource.EVENT_KEY;
import static org.fulib.yaml.EventSource.EVENT_TYPE;

public class ShopProxy {

    private ScheduledExecutorService executor;
    private EventSource eventSource;
    private EventFiler eventFiler;

    private boolean testMode;

    public ShopProxy() {
        this(false);
    }

    public ShopProxy(boolean testMode) {
        this.testMode = testMode;
        executor = Executors.newSingleThreadScheduledExecutor();
        eventSource = new EventSource();

        eventFiler = new EventFiler(eventSource)
                .setHistoryFileName(SHOP_PROXY_LOG);

        String yaml = eventFiler.loadHistory();
        if (yaml != null) {
//            ArrayList<LinkedHashMap<String, String>> eventList = new Yamler().decodeList(yaml);
            eventSource.append(yaml);
        }

        eventFiler.storeHistory();
        eventFiler.startEventLogging();

    }

    public String getEvents() {
        return eventFiler.loadHistory();
    }

    /**
     * Saves the "request" as a log and pings the ShopServer to inform it about new events.
     *
     * @param lotID       The id of the lot.
     * @param productName The name of the product in this lot.
     * @param size        The amount of the product.
     */
    public void addLotToShop(String lotID, String productName, int size) {

        LinkedHashMap<String, String> event = new LinkedHashMap<>();
        event.put(EVENT_TYPE, ADD_TO_SHOP_EVENT);
        event.put(EVENT_KEY, lotID);
        event.put(PRODUCT_NAME, productName);
        event.put(SIZE, "" + size);

        eventSource.append(event);

        if (!testMode) {
            String yaml = "ping: p1";

            sendRequest("http://server_shop:" + SHOP_SERVER_PORT + "/ping", yaml);
        }

    }

    @Deprecated
    private void oldAddLotToServer(String lotID, String productName, int size) {
        try {
            URL url = new URL("http://localhost:8000/addLotToShop");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");

            con.setDoOutput(true);

            LinkedHashMap<String, String> msgMap = new LinkedHashMap<>();
            msgMap.put(LOT_ID, lotID);
            msgMap.put(PRODUCT_NAME, productName);
            msgMap.put(SIZE, "" + size);

            Gson gson = new Gson();
            String json = gson.toJson(msgMap);

            byte[] out = json.getBytes();
            int length = out.length;

            con.setFixedLengthStreamingMode(length);
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.connect();

            try {
                OutputStream os = con.getOutputStream();
                os.write(out);
                os.flush();
                os.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            InputStream inputStream = con.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(inputStream));

            StringBuilder sb = new StringBuilder();
            while (true) {
                String line = buf.readLine();
                if (line == null) {
                    break;
                }

                sb.append(line).append("\n");
            }

            String response = sb.toString();

//            System.out.println(response);

            buf.close();

        } catch (Exception e) {
            e.printStackTrace();
            executor.schedule(
                    () -> addLotToShop(lotID, productName, size), 60, TimeUnit.SECONDS);
        }
    }
}
