package ha08;

import com.sun.net.httpserver.HttpExchange;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class Utils {

    public static final String WAREHOUSE_BUILDER_LOG = "database/ha08/Warehouse51.yaml";
    public static final String SHOP_BUILDER_LOG = "database/ha08/Shop42.yaml";
    public static final String SHOP_PROXY_LOG = "database/ha08/ShopProxy.yaml";
    public static final String WAREHOUSE_PROXY_LOG = "database/ha08/WarehouseProxy.yaml";

    public static final String ADD_LOT_TO_STOCK = "addLotToStock";
    public static final String PRODUCT_NAME = "productName";
    public static final String SIZE = "size";

    public static final String SHIP_ORDER = "shipOrder";
    public static final String ADDRESS = "address";

    public static final String ADD_TO_SHOP_EVENT = "addToShopEvent";

    public static final String ADD_CUSTOMER = "addCustomer";

    public static final String CUSTOMER_NAME = "customerName";
    public static final String ORDER_PRODUCT = "orderProduct";

    public static final String LOT_ID = "lotID";
    public static final String ADD_LOT_TO_SHOP = "addLotToShop";

    public static final String ORDER_ID = "orderID";

    public static final int WAREHOUSE_SERVER_PORT = 8001;
    public static final int SHOP_SERVER_PORT = 8000;

    // for testing; wait till the container is running
    public static String sendRequest(String url, String postYaml) {
        return sendRequest(url, postYaml, true);
    }

    /**
     * Send a http request with the given body to the given address.
     *
     * @param url      The address where the request is send too.
     * @param postYaml The body of the message.
     * @return Returns a String containing the answer of the request. Or null if an error occurred.
     */
    public static String sendRequest(String url, String postYaml, boolean silentMode) {
        if (!silentMode) {
            Logger.getGlobal().info("\n" +
                    "Server::sendRequest\n" +
                    "url: " + url + "\n" +
                    "postYaml: " + postYaml);
        }

        try {
            URL theUrl = new URL(url);
            URLConnection con = theUrl.openConnection();
            HttpURLConnection http = (HttpURLConnection) con;
            http.setRequestMethod("PUT");
            http.setDoOutput(true);

            byte[] out = postYaml.getBytes(StandardCharsets.UTF_8);
            int length = out.length;

            http.setFixedLengthStreamingMode(length);
            http.setRequestProperty("Content-Type", "application/yaml; charset=UTF-8");
            try {
                http.connect();
            } catch (ConnectException e) {
                return "Unavailable.";
            }

            try (OutputStream os = http.getOutputStream()) {
                os.write(out);
            }

            InputStream inputStream = http.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();

            while (true) {
                String line = buf.readLine();
                if (line == null) {
                    break;
                }

                sb.append(line).append("\n");
            }

            return sb.toString();

        } catch (Exception e) {
            if (!silentMode) {
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * Sends an answer to a received http request.
     *
     * @param exchange The request to answer too.
     * @param response The body of the reponse.
     */
    public static void writeAnswer(HttpExchange exchange, String response) {
        try {
            byte[] bytes = response.getBytes();
            exchange.sendResponseHeaders(200, bytes.length);
            OutputStream os = exchange.getResponseBody();
            os.write(bytes);
            os.close();
        } catch (IOException e) {
            Logger.getGlobal().info("could not answer");
        }
    }

    /**
     * Extracts the body of a given HttpExchange.
     *
     * @param exchange The exchange to get the body from.
     * @return A String containing the body of the request.
     */
    public static String getBody(HttpExchange exchange) {
        try {
            URI requestURI = exchange.getRequestURI();
            InputStream requestBody = exchange.getRequestBody();
            BufferedReader buf = new BufferedReader(new InputStreamReader(requestBody, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();

            while (true) {
                String line = buf.readLine();
                if (line == null) {
                    break;
                }

                sb.append(line).append("\n");
            }

            String yaml = sb.toString();
            return yaml;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;


    }
}
