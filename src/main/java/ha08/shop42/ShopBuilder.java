package ha08.shop42;

import ha08.shop42.model.Shop42;
import ha08.shop42.model.ShopCustomer;
import ha08.shop42.model.ShopOrder;
import ha08.shop42.model.ShopProduct;
import org.fulib.yaml.EventFiler;
import org.fulib.yaml.EventSource;
import org.fulib.yaml.Yamler;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static ha08.Utils.*;
import static org.fulib.yaml.EventSource.EVENT_KEY;
import static org.fulib.yaml.EventSource.EVENT_TYPE;

public class ShopBuilder {

    private EventSource eventSource;


    private Shop42 shop42;
    public WarehouseProxy warehouseProxy;

    public ShopBuilder() {

        shop42 = new Shop42();
        warehouseProxy = new WarehouseProxy();
        eventSource = new EventSource();

        // start logging
        EventFiler eventFiler = new EventFiler(eventSource)
                .setHistoryFileName(SHOP_BUILDER_LOG);

        // restore stuff from log
        String yaml = eventFiler.loadHistory();
        if (yaml != null) {
            ArrayList<LinkedHashMap<String, String>> eventList
                    = new Yamler().decodeList(yaml);
            applyEvents(eventList);
        }

        eventFiler.startEventLogging();
    }

    public void applyEvents(ArrayList<LinkedHashMap<String, String>> eventList) {
        // TODO change to chanin of responsibility
        for (LinkedHashMap<String, String> event : eventList) {
            if (ADD_TO_SHOP_EVENT.equals(event.get(EVENT_TYPE))) {

                String lotID = event.get(EVENT_KEY);
                String productName = event.get(PRODUCT_NAME);
                int size = Integer.parseInt(event.get(SIZE));
                addLotToShop(lotID, productName, size);
            } else if (ADD_CUSTOMER.equals(event.get(EVENT_TYPE))) {

                String name = event.get(EVENT_KEY);
                String address = event.get(ADDRESS);
                addCustomer(name, address);
            } else if (ORDER_PRODUCT.equals(event.get(EVENT_TYPE))) {

                String orderID = event.get(EVENT_KEY);
                String productName = event.get(PRODUCT_NAME);
                String customerName = event.get(CUSTOMER_NAME);

                orderProduct(orderID, productName, customerName);
            }
        }
    }

    public void addLotToShop(String lotID, String productName, int size) {
        // check if log contains lotID
        LinkedHashMap<String, String> event = eventSource.getEvent(lotID);
        if (event != null) {
            // this is a doubled call to this method then
            return;
        }

        ShopProduct product = getOrCreateProduct(productName);
        int currentSize = product.getStock();
        product.setStock(currentSize + size);

        // log this method call
        event = new LinkedHashMap<>();
        event.put(EVENT_TYPE, ADD_TO_SHOP_EVENT);
        event.put(EVENT_KEY, lotID);
        event.put(PRODUCT_NAME, productName);
        event.put(SIZE, "" + size);

        eventSource.append(event);
    }

    public void addCustomer(String name, String address) {

        // logging
        LinkedHashMap<String, String> event = new LinkedHashMap<>();
        event.put(EVENT_TYPE, ADD_CUSTOMER);
        event.put(EVENT_KEY, name);
        event.put(ADDRESS, address);

        eventSource.append(event);

        ShopCustomer customer = getOrCreateCustomer(name);
        customer.setShop(shop42);
        customer.setAddress(address);

    }

    private ShopCustomer getOrCreateCustomer(String name) {

        for (ShopCustomer c : shop42.getCustomers()) {
            if (c.getName().equals(name)) {
                return c;
            }
        }

        ShopCustomer newCustomer = new ShopCustomer();
        newCustomer.setName(name);

        return newCustomer;
    }

    public void orderProduct(String orderID, String productName, String customerName) {

        // logging
        LinkedHashMap<String, String> event = new LinkedHashMap<>();
        event.put(EVENT_KEY, orderID);
        event.put(EVENT_TYPE, ORDER_PRODUCT);
        event.put(PRODUCT_NAME, productName);
        event.put(CUSTOMER_NAME, customerName);

        eventSource.append(event);

        // get product
        ShopProduct product = getProduct(productName);

        if (product == null) {
            System.err.println("ERROR: Product " + productName + " unknown.");
            return;
        }

        // get customer
        ShopCustomer customer = getCustomer(customerName);

        if (customer == null) {
            System.err.println("ERROR: Customer " + customerName + " unknown.");
            return;
        }

        // place order
        ShopOrder order = getOrCreateOrder(orderID);
        order.setShop(shop42);
        order.setShopProduct(product);
        order.setCustomer(customer);

        warehouseProxy.shipOrder(order.getShopProduct().getName(), customer.getAddress(), orderID);

        // FIXME this could lead to inconsistency
        int oldStock = product.getStock();
        product.setStock(oldStock - 1);

    }

    private ShopOrder getOrCreateOrder(String orderID) {

        for (ShopOrder o : shop42.getOrders()) {
            if (o.getId().equals(orderID)) {
                return o;
            }
        }

        ShopOrder newOrder = new ShopOrder();
        newOrder.setId(orderID);

        return newOrder;
    }

    private ShopCustomer getCustomer(String customerName) {

        for (ShopCustomer c : shop42.getCustomers()) {
            if (c.getName().equals(customerName)) {
                return c;
            }
        }

        return null;

    }

    /**
     * Checks if the product is already known to the shop. Returns the existing product or a new product.
     *
     * @param productName The name to search for products
     * @return Existing product if it exists. A new product if no product exists with the given name.
     */
    private ShopProduct getOrCreateProduct(String productName) {

        String productID = productName.replaceAll("\\W", "");

        // search if product with id exists
        for (ShopProduct p : shop42.getProducts()) {
            if (p.getId().equals(productID)) {
                return p;
            }
        }

        // else return new product
        ShopProduct product = new ShopProduct();
        product.setShop(shop42);
        product.setId(productID);
        product.setName(productName);

        return product;
    }

    private ShopProduct getProduct(String productName) {

        String productID = productName.replaceAll("\\W", "");

        for (ShopProduct p : shop42.getProducts()) {
            if (p.getId().equals(productID)) {
                return p;
            }
        }

        return null;
    }

    /**
     * Getter for Products field of Shop42 class.
     *
     * @return ArrayList of ShopProduct instances that are known to the shop42.
     */
    public ArrayList<ShopProduct> getProducts() {
        return shop42.getProducts();
    }

    public ArrayList<ShopCustomer> getCustomers() {
        return shop42.getCustomers();
    }

    public ArrayList<ShopOrder> getOrders() {
        return shop42.getOrders();
    }

}
