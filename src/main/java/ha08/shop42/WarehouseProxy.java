package ha08.shop42;

import com.google.gson.Gson;
import org.fulib.yaml.EventFiler;
import org.fulib.yaml.EventSource;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static ha08.Utils.*;
import static org.fulib.yaml.EventSource.EVENT_KEY;
import static org.fulib.yaml.EventSource.EVENT_TYPE;

public class WarehouseProxy {

    private ScheduledExecutorService executor;
    private boolean testMode;
    private EventSource eventSource;
    private EventFiler eventFiler;

    public WarehouseProxy() {
        this(false);
    }

    public WarehouseProxy(boolean testMode) {
        this.testMode = testMode;
        executor = Executors.newSingleThreadScheduledExecutor();
        eventSource = new EventSource();

        eventFiler = new EventFiler(eventSource)
                .setHistoryFileName(WAREHOUSE_PROXY_LOG);

        String yaml = eventFiler.loadHistory();
        if (yaml != null) {
            eventSource.append(yaml);
        }

        eventFiler.storeHistory();
        eventFiler.startEventLogging();
    }

    public String getEvents() {
        return eventFiler.loadHistory();
    }

    /**
     * Logs the request and pings the WarehouseServer to inform it about the new event.
     *
     * @param productName The name of the product ordered.
     * @param address     The address of the customer.
     * @param orderID     The id of the order.
     */
    public void shipOrder(String productName, String address, String orderID) {

        LinkedHashMap<String, String> event = new LinkedHashMap<>();
        event.put(EVENT_TYPE, SHIP_ORDER);
        event.put(EVENT_KEY, orderID);
        event.put(PRODUCT_NAME, productName);
        event.put(ADDRESS, address);

        eventSource.append(event);

        if (!testMode) {
            String yaml = "ping: p2";
            sendRequest("http://server_warehouse:" + WAREHOUSE_SERVER_PORT + "/ping", yaml);
        }

    }

    @Deprecated
    public void oldShipOrder(String productName, String address, String orderID) {
        try {
            URL url = new URL("http://localhost:" + WAREHOUSE_SERVER_PORT + "/shipOrder");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");

            con.setDoOutput(true);

            LinkedHashMap<String, String> msgMap = new LinkedHashMap<>();
            msgMap.put(ORDER_ID, orderID);
            msgMap.put(PRODUCT_NAME, productName);
            msgMap.put(ADDRESS, address);

            Gson gson = new Gson();
            String json = gson.toJson(msgMap);

            byte[] out = json.getBytes();
            int length = out.length;

            con.setFixedLengthStreamingMode(length);
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.connect();

            try {
                OutputStream os = con.getOutputStream();
                os.write(out);
                os.flush();
                os.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            InputStream inputStream = con.getInputStream();
            BufferedReader buf = new BufferedReader(new InputStreamReader(inputStream));

            StringBuilder sb = new StringBuilder();
            while (true) {
                String line = buf.readLine();
                if (line == null) {
                    break;
                }

                sb.append(line).append("\n");
            }

            String response = sb.toString();

//            System.out.println(response);

            buf.close();

        } catch (Exception e) {
            e.printStackTrace();
            executor.schedule(
                    () -> shipOrder(productName, address, orderID), 60, TimeUnit.SECONDS);
        }
    }
}
