package ha08.shop42.model;

import java.beans.PropertyChangeSupport;

import java.beans.PropertyChangeListener;

public class ShopOrder  
{

   public static final String PROPERTY_id = "id";

   private String id;

   public String getId()
   {
      return id;
   }

   public ShopOrder setId(String value)
   {
      if (value == null ? this.id != null : ! value.equals(this.id))
      {
         String oldValue = this.id;
         this.id = value;
         firePropertyChange("id", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_shop = "shop";

   private Shop42 shop = null;

   public Shop42 getShop()
   {
      return this.shop;
   }

   public ShopOrder setShop(Shop42 value)
   {
      if (this.shop != value)
      {
         Shop42 oldValue = this.shop;
         if (this.shop != null)
         {
            this.shop = null;
            oldValue.withoutOrders(this);
         }
         this.shop = value;
         if (value != null)
         {
            value.withOrders(this);
         }
         firePropertyChange("shop", oldValue, value);
      }
      return this;
   }


   protected PropertyChangeSupport listeners = null;

   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null)
      {
         listeners.firePropertyChange(propertyName, oldValue, newValue);
         return true;
      }
      return false;
   }

   public boolean addPropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(listener);
      return true;
   }

   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(propertyName, listener);
      return true;
   }

   public boolean removePropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(listener);
      }
      return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(propertyName, listener);
      }
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();

      result.append(" ").append(this.getId());


      return result.substring(1);
   }

   public void removeYou()
   {
      this.setShop(null);
      this.setShopProduct(null);
      this.setCustomer(null);

   }



   public static final String PROPERTY_shopProduct = "shopProduct";

   private ShopProduct shopProduct = null;

   public ShopProduct getShopProduct()
   {
      return this.shopProduct;
   }

   public ShopOrder setShopProduct(ShopProduct value)
   {
      if (this.shopProduct != value)
      {
         ShopProduct oldValue = this.shopProduct;
         if (this.shopProduct != null)
         {
            this.shopProduct = null;
            oldValue.withoutOrders(this);
         }
         this.shopProduct = value;
         if (value != null)
         {
            value.withOrders(this);
         }
         firePropertyChange("shopProduct", oldValue, value);
      }
      return this;
   }



   public static final String PROPERTY_customer = "customer";

   private ShopCustomer customer = null;

   public ShopCustomer getCustomer()
   {
      return this.customer;
   }

   public ShopOrder setCustomer(ShopCustomer value)
   {
      if (this.customer != value)
      {
         ShopCustomer oldValue = this.customer;
         if (this.customer != null)
         {
            this.customer = null;
            oldValue.withoutOrders(this);
         }
         this.customer = value;
         if (value != null)
         {
            value.withOrders(this);
         }
         firePropertyChange("customer", oldValue, value);
      }
      return this;
   }



}