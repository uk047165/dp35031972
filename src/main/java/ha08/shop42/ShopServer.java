package ha08.shop42;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import ha08.shop42.model.ShopProduct;
import org.fulib.yaml.EventSource;
import org.fulib.yaml.Yamler;

import java.io.*;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import static ha08.Utils.*;
import static org.fulib.yaml.EventSource.EVENT_KEY;
import static org.fulib.yaml.EventSource.EVENT_TYPE;

public class ShopServer {


    private static ExecutorService executor;
    private static ExecutorService threadPool;
    private static long lastKnownWarehouseEventTime = 0;
    public static HttpServer server;
    private static ShopBuilder builder;

    public static void main(String[] args) {
        try {
            builder = new ShopBuilder();

            executor = Executors.newSingleThreadExecutor();
            threadPool = Executors.newCachedThreadPool();

            server = HttpServer.create(new InetSocketAddress(SHOP_SERVER_PORT), 0);
            server.setExecutor(executor);
            HttpContext context = server.createContext("/ping");
            context.setHandler(ShopServer::handleWarehousePing);

            HttpContext proxyContext = server.createContext("/getWarehouseEvents");
            proxyContext.setHandler(ShopServer::handleWarehouseProxyRequest);

            HttpContext addCustomerContext = server.createContext("/getLastProduct");
            addCustomerContext.setHandler(ShopServer::getLastProduct);

            server.start();

            // FIXME this is producing exceptions, how to handle this better?
            retrieveNewEventsFromWarehouse();
        } catch (IOException e) {

            if (e instanceof ConnectException) {
                Logger.getGlobal().info("ConnectionException: Server might not be available!");
                return;
            }

            e.printStackTrace();
        }
    }

    private static void getLastProduct(HttpExchange e) {
        try {
            ShopProduct p = builder.getProducts().get(builder.getProducts().size() - 1);
            String name = p.getName();
            int stock = p.getStock();

            LinkedHashMap<String, String> map = new LinkedHashMap<>();
            map.put(EVENT_KEY, "someKey");
            map.put(EVENT_TYPE, "testProduct");
            map.put(PRODUCT_NAME, name);
            map.put(SIZE, "" + stock);
            String s = EventSource.encodeYaml(map);
            writeAnswer(e, s);
        } catch (Exception ex) {
            writeAnswer(e, ex.getMessage());
        }

    }

    // for test purpose only
    private static void testOrderProduct(String orderID, String productName, String customerName) {
        builder.orderProduct(orderID, productName, customerName);
    }

    private static void retrieveNewEventsFromWarehouse() {
        String wareHouseEvents = sendRequest("http://server_warehouse:" + WAREHOUSE_SERVER_PORT + "/getShopEvents",
                "lastKnown: " + lastKnownWarehouseEventTime); // FIXME change lastKnown to String with datefromat iso date format -> like in yaml
        // getShopEvents ::= get events the shop should know from the warehouseProxy

        Logger.getGlobal().info("ShopServer:\n" +
                "retrieveNewEventsFromWarehouse:\n" + wareHouseEvents);

        if (wareHouseEvents != null && builder != null) {
            ArrayList<LinkedHashMap<String, String>> eventList = new Yamler().decodeList(wareHouseEvents);

            executor.execute(() -> builder.applyEvents(eventList));
        }
    }

    /**
     * This handles the request of the WarehouseServer to retrieve all events from the ShopProxy.
     * Sends all events of the WarehouseProxy as an answer to the received exchange.
     *
     * @param exchange The exchange to answer to.
     * @throws IOException
     */
    private static void handleWarehouseProxyRequest(HttpExchange exchange) throws IOException {
        String yaml;

        if (builder != null) {
            yaml = builder.warehouseProxy.getEvents();
            System.out.println("Shop Server: send\n" + yaml);
        } else {
            yaml = "Unavailable.";
        }

        writeAnswer(exchange, yaml);
    }

    /**
     * Handles when a ping request arrives from the WarehouseServer.
     * This means that a new event occurred on the WarehouseServer
     * and we should retrieve all new events and apply them here.
     * The method automatically retrieves all events from the server by calling retrieveNewEventsFromWarehouse.
     *
     * @param exchange The request that contains the ping message.
     * @throws IOException
     */
    private static void handleWarehousePing(HttpExchange exchange) throws IOException {
        String body = getBody(exchange);
        System.out.println("Shop Server: got " + body);
        writeAnswer(exchange, "OK");

        // retrieve new events
        retrieveNewEventsFromWarehouse();
    }

    public static ShopBuilder getBuilder() {
        return builder;
    }

    public static void setBuilder(ShopBuilder b) {
        builder = b;
        retrieveNewEventsFromWarehouse();
    }

    @Deprecated
    public static void oldStartServer() {
//        try {
//            server = HttpServer.create(new InetSocketAddress(8000), 0);
//
//            server.createContext("/addLotToShop", new addLotToShopHandler());
//
//            server.setExecutor(null);
//
//            server.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    @Deprecated
    static class oldAddLotToShopHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            InputStream requestBody = httpExchange.getRequestBody();
            BufferedReader buf = new BufferedReader(new InputStreamReader(requestBody, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();

            while (true) {
                String line = buf.readLine();
                if (line == null) {
                    break;
                }

                sb.append(line).append("\n");
            }

            //  apply events in builder
            String json = sb.toString();

            Gson gson = new Gson();
            Map<String, String> message = gson.fromJson(json, Map.class);

            builder.addLotToShop(message.get(LOT_ID), message.get(PRODUCT_NAME), Integer.parseInt(message.get(SIZE)));

            // send ok response
            String response = "OK" + httpExchange.getRequestURI() + "\nMessage: " + json + " arrived!";
            httpExchange.sendResponseHeaders(200, response.getBytes().length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();

        }

    }
}
